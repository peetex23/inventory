<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    //
    protected $table = 'purchases';

    protected $fillable = [
    	"nomor_masuk",
    	"distributor_id",
    	"nomor_faktur",
    	"tanggal_faktur",
    	"status",
    	"total_faktur",
    	"total_pajak",
    	"total_diskon"
    ];

    public function distributors()
    {
    	return $this->hasMany('Distributor','distributor_id');
    }


	public function purchase_details()
	{
		return $this->belongsTo('PurchaseDetail', 'purchase_id');
	}
}
