<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Factory extends Model
{
    protected $fillable = [
    		"kode",
			"nama",
			"alamat",
			"telp_kantor",
			"narahubung",
			"telp_narahubung",
			"aktif"];

	public function purchase_details()
	{
		return $this->belongsTo('PurchaseDetail', 'factory_id');
	}
}
