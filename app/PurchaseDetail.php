<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseDetail extends Model
{
    protected $table = 'purchase_details';

    protected $fillable = [
	    'purchase_id'
		'factory_id'
		'item_id'
		'jml_masuk'
		'harga_satuan'
		'diskon_persen'
		'diskon_nominal'
		'harga_netto'
		'pajak'
	];

    public function purchases()
    {
    	return $this->hasMany('Purchase','purchase_id');
    }

    public function items()
    {
    	return $this->hasMany('Item','item_id');
    }

    public function factories()
    {
    	return $this->hasMany('Factory','factory_id');
    }

}
