<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Distributor extends Model
{
    protected $fillable = [
    		"kode",
			"nama",
			"alamat",
			"telp_kantor",
			"narahubung",
			"telp_narahubung",
			"aktif"];

	public function purchases()
	{
		return $this->belongsTo('Purchase', 'distributor_id');
	}
}
