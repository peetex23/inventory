<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::all();
        return view('items.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('items.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode'=>'required',
            'nama'=>'required'
        ]);
        $item = new Item([
            'kode'          => $request->get('kode'),
            'nama'          => $request->get('nama'),
            'merk'          => $request->get('merk'),
            'model'         => $request->get('model'),
            'spesifikasi'   => preg_replace("/\n/", "<br>", $request->get('spesifikasi')),
            'satuan_kecil'  => $request->get('satuan_kecil'),
            'kemasan_besar' => $request->get('kemasan_besar'),
            'aktif'         => $request->get('aktif')
        ]);
        $item->save();
        return redirect('/items')->with('success', 'Item berhasil disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::find($id);
        return view('items.edit', compact('item')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kode'=>'required',
            'nama'=>'required'
        ]);

        $item = Item::find($id);
        $item->kode         =  $request->get('kode');
        $item->nama         = $request->get('nama');
        $item->merk         = $request->get('merk');
        $item->model        = $request->get('model');
        $item->spesifikasi  = preg_replace("/\n/", "<br>", $request->get('spesifikasi'));
        $item->satuan_kecil = $request->get('satuan_kecil');
        $item->kemasan_besar= $request->get('kemasan_besar');
        $item->aktif        = $request->get('aktif');
        $item->save();
        return redirect('/items')->with('success', 'Item berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::find($id);
        $item->delete();

        return redirect('/items')->with('success', 'Item berhasil dihapus!');
    }
}
