<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Distributor;

class DistributorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $distributors = Distributor::all();
        return view('distributors.index', compact('distributors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('distributors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode'=>'required',
            'nama'=>'required'
        ]);
        $distributor = new Distributor([
            'kode'          => $request->get('kode'),
            'nama'          => $request->get('nama'),
            'alamat'        => preg_replace("/\n/", "<br>", $request->get('alamat')),
            'telp_kantor'   => $request->get('telp_kantor'),
            'narahubung'    => $request->get('narahubung'),
            'telp_narahubung' => $request->get('telp_narahubung'),
            'aktif'         => $request->get('aktif')
        ]);
        $distributor->save();
        return redirect('/distributors')->with('success', 'Distributor berhasil disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $distributor = Distributor::find($id);
        return view('distributors.edit', compact('distributor')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kode'=>'required',
            'nama'=>'required'
        ]);

        $distributor = Distributor::find($id);
        $distributor->kode          =  $request->get('kode');
        $distributor->nama          = $request->get('nama');
        $distributor->alamat        = preg_replace("/\n/", "<br>", $request->get('alamat'));
        $distributor->telp_kantor   = $request->get('telp_kantor');
        $distributor->narahubung    = $request->get('narahubung');
        $distributor->telp_narahubung = $request->get('telp_narahubung');
        $distributor->aktif         = $request->get('aktif');
        $distributor->save();
        return redirect('/distributors')->with('success', 'Distributor berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $distributor = Distributor::find($id);
        $distributor->delete();

        return redirect('/distributors')->with('success', 'Distributor berhasil dihapus!');
    }
}
