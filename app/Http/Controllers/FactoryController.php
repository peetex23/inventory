<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factory;

class FactoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $factories = Factory::all();
        return view('factories.index', compact('factories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('factories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode'=>'required',
            'nama'=>'required'
        ]);
        $factory = new Factory([
            'kode'          => $request->get('kode'),
            'nama'          => $request->get('nama'),
            'alamat'        => preg_replace("/\n/", "<br>", $request->get('alamat')),
            'telp_kantor'   => $request->get('telp_kantor'),
            'narahubung'    => $request->get('narahubung'),
            'telp_narahubung' => $request->get('telp_narahubung'),
            'aktif'         => $request->get('aktif')
        ]);
        $factory->save();
        return redirect('/factories')->with('success', 'Pabrik berhasil disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $factory = Factory::find($id);
        return view('factories.edit', compact('factory')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kode'=>'required',
            'nama'=>'required'
        ]);

        $factory = Factory::find($id);
        $factory->kode         =  $request->get('kode');
        $factory->nama         = $request->get('nama');
        $factory->alamat  = preg_replace("/\n/", "<br>", $request->get('alamat'));
        $factory->telp_kantor         = $request->get('telp_kantor');
        $factory->narahubung        = $request->get('narahubung');
        $factory->telp_narahubung = $request->get('telp_narahubung');
        $factory->aktif        = $request->get('aktif');
        $factory->save();
        return redirect('/factories')->with('success', 'Pabrik berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $factory = Factory::find($id);
        $factory->delete();

        return redirect('/factories')->with('success', 'Pabrik berhasil dihapus!');
    }
}
