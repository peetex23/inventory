<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    //
    protected $fillable = [
    		'kode',
			'nama',
			'merk',
			'model',
			'spesifikasi',
			'satuan_kecil',
			'kemasan_besar',
			'aktif'];


	public function purchase_details()
	{
		return $this->belongsTo('PurchaseDetail', 'item_id');
	}
}
