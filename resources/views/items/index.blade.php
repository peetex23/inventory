@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-12">
        @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}  
        </div>
        @endif
        <div class="row">
            <div class="col-sm-8">
                <h1 class="display-3">Daftar Item</h1>    
            </div>
            <div class="col-sm-4">
                <a href="{{ route('items.create')}}" class="btn btn-primary"></i>Tambah</a>
            </div>
        </div>
        <table class="table table-striped">
        <thead>
            <tr>
                <td>Kode</td>
                <td>Nama</td>
                <td>Merk</td>
                <td>Model</td>
                <td>Spesifikasi</td>
                <td>Satuan Kecil</td>
                <td>Kemasan Besar</td>
                <td>Status</td>
                <td colspan = 2>Actions</td>
            </tr>
        </thead>
        <tbody>
            @foreach($items as $item)
            <tr>
                <td>{{$item->kode}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->merk}}</td>
                <td>{{$item->model}}</td>
                <td>{!! $item->spesifikasi !!}</td>
                <td>{{$item->satuan_kecil}}</td>
                <td>{{$item->kemasan_besar}}</td>
                <td>{{($item->aktif == '1')?"Aktif":"Non-Aktif"}}</td>
                <td>
                    <a href="{{ route('items.edit',$item->id)}}" class="btn btn-primary">Edit</a>
                </td>
                <td>
                    <form action="{{ route('items.destroy', $item->id)}}" method="post">
                      @csrf
                      @method('DELETE')
                      <button class="btn btn-danger" type="submit" onclick="if(confirm('Lanjutkan hapus data?')) {return true;} else {return false;}">Hapus</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
        </table>
    <div>
</div>
@endsection