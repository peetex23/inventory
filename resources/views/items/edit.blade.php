@extends('layouts.app')

@section('content')
<div class="row">
 	<div class="col-sm-6 offset-sm-2">
    	<h1 class="display-3">Ubah data item</h1>
  		<div>
    		@if ($errors->any())
      			<div class="alert alert-danger">
        		<ul>
            		@foreach ($errors->all() as $error)
              		<li>{{ $error }}</li>
            		@endforeach
        		</ul>
      			</div><br>
    		@endif
      		<form method="post" action="{{ route('items.update', $item->id) }}">
      			@method('PATCH')
          		@csrf
          		<div class="form-group">    
              		<label for="kode">Kode:</label>
              		<input type="text" class="form-control" name="kode" value="{{$item->kode}}" />
	          	</div>
	          	<div class="form-group">
	              	<label for="nama">Nama:</label>
	              	<input type="text" class="form-control" name="nama" value="{{$item->nama}}"/>
	         	</div>
	          	<div class="form-group">
	              	<label for="merk">Merk:</label>
	              	<input type="text" class="form-control" name="merk" value="{{$item->merk}}"/>
	          	</div>
	          	<div class="form-group">
	              	<label for="model">Model:</label>
	              	<input type="text" class="form-control" name="model" value="{{$item->model}}"/>
	          	</div>
	          	<div class="form-group">
	              	<label for="spesifikasi">Spesifikasi:</label>
	              	<textarea class="form-control" name="spesifikasi" cols="30" rows="4">{{str_replace("<br>","\n",$item->spesifikasi)}}</textarea>
	          	</div>
	          	<div class="form-group">
	              	<label for="satuan_kecil">Satuan Kecil:</label>
	              	<input type="text" class="form-control" name="satuan_kecil" value="{{$item->satuan_kecil}}"/>
	          	</div>
	          	<div class="form-group">
	              	<label for="kemasan_besar">Kemasan Besar:</label>
	              	<input type="text" class="form-control" name="kemasan_besar" value="{{$item->kemasan_besar}}"/>
	          	</div>
	          	<div class="form-group">
	              	<label for="aktif">Aktif:</label>
	              	<select class="form-control" name="aktif">
	              		<option value="1" {{($item->aktif == '1')?'selected':''}}>Aktif</option>
	              		<option value="0" {{($item->aktif == '0')?'selected':''}}>Non-Aktif</option>
	              	</select>
	          	</div>
	          	<button type="submit" class="btn btn-primary">Ubah Data</button>&emsp;
	          	<a href="{{route('items.index')}}" class="btn btn-danger">Batal</a>
	     	</form>
  		</div>
	</div>
</div>
@endsection