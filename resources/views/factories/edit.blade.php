@extends('layouts.app')

@section('content')
<div class="row">
 	<div class="col-sm-6 offset-sm-2">
    	<h1 class="display-3">Ubah data pabrik</h1>
  		<div>
    		@if ($errors->any())
      			<div class="alert alert-danger">
        		<ul>
            		@foreach ($errors->all() as $error)
              		<li>{{ $error }}</li>
            		@endforeach
        		</ul>
      			</div><br>
    		@endif
      		<form method="post" action="{{ route('factories.update', $factory->id) }}">
      			@method('PATCH')
          		@csrf
          		<div class="form-group">    
              		<label for="kode">Kode:</label>
              		<input type="text" class="form-control" name="kode" value="{{$factory->kode}}" />
	          	</div>
	          	<div class="form-group">
	              	<label for="nama">Nama:</label>
	              	<input type="text" class="form-control" name="nama" value="{{$factory->nama}}"/>
	         	</div>
	          	<div class="form-group">
	              	<label for="alamat">Alamat:</label>
	              	<textarea class="form-control" name="alamat" cols="30" rows="4">{{str_replace("<br>","\n",$factory->alamat)}}</textarea>
	          	</div>
	          	<div class="form-group">
	              	<label for="telp_kantor">Telp Kantor:</label>
	              	<input type="text" class="form-control" name="telp_kantor" value="{{$factory->telp_kantor}}"/>
	          	</div>
	          	<div class="form-group">
	              	<label for="narahubung">Contact Person:</label>
	              	<input type="text" class="form-control" name="narahubung" value="{{$factory->narahubung}}"/>
	          	</div>
	          	<div class="form-group">
	              	<label for="telp_narahubung">Telp. CP.:</label>
	              	<input type="text" class="form-control" name="telp_narahubung" value="{{$factory->telp_narahubung}}"/>
	          	</div>
	          	<div class="form-group">
	              	<label for="aktif">Aktif:</label>
	              	<select class="form-control" name="aktif">
	              		<option value="1" {{($factory->aktif == '1')?'selected':''}}>Aktif</option>
	              		<option value="0" {{($factory->aktif == '0')?'selected':''}}>Non-Aktif</option>
	              	</select>
	          	</div>
	          	<button type="submit" class="btn btn-primary">Ubah Data</button>&emsp;
	          	<a href="{{route('factories.index')}}" class="btn btn-danger">Batal</a>
	     	</form>
  		</div>
	</div>
</div>
@endsection