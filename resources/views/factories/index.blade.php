@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-12">
        @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}  
        </div>
        @endif
        <div class="row">
            <div class="col-sm-8">
                <h1 class="display-3">Daftar Pabrik</h1>    
            </div>
            <div class="col-sm-4">
                <a href="{{ route('factories.create')}}" class="btn btn-primary"></i>Tambah</a>
            </div>
        </div>
        <table class="table table-striped">
        <thead>
            <tr>
                <td>Kode</td>
                <td>Nama</td>
                <td>Alamat</td>
                <td>Telp. Kantor</td>
                <td>Contact Person</td>
                <td>Telp. CP.</td>
                <td>Status</td>
                <td colspan = 2>Actions</td>
            </tr>
        </thead>
        <tbody>
            @foreach($factories as $factory)
            <tr>
                <td>{{$factory->kode}}</td>
                <td>{{$factory->nama}}</td>
                <td>{!! $factory->alamat !!}</td>
                <td>{{$factory->telp_kantor}}</td>
                <td>{{$factory->narahubung}}</td>
                <td>{{$factory->telp_narahubung}}</td>
                <td>{{($factory->aktif == '1')?"Aktif":"Non-Aktif"}}</td>
                <td>
                    <a href="{{ route('factories.edit',$factory->id)}}" class="btn btn-primary">Edit</a>
                </td>
                <td>
                    <form action="{{ route('factories.destroy', $factory->id)}}" method="post">
                      @csrf
                      @method('DELETE')
                      <button class="btn btn-danger" type="submit" onclick="if(confirm('Lanjutkan hapus data?')) {return true;} else {return false;}">Hapus</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
        </table>
    <div>
</div>
@endsection