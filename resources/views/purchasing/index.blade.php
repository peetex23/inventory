@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-12">
        @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}  
        </div>
        @endif
        <div class="row">
            <div class="col-sm-8">
                <h1 class="display-3">Daftar Pembelian</h1>    
            </div>
            <div class="col-sm-4">
                <a href="{{ route('purchasing.create')}}" class="btn btn-primary"></i>Tambah</a>
            </div>
        </div>
        <table class="table table-striped">
        <thead>
            <tr>
                <td>No</td>
                <td>Tgl Beli</td>
                <td>Nomor Pembelian</td>
                <td>Distributor</td>
                <td>Nomor Faktur</td>
                <td>Tanggal Faktur</td>
                <td>Total</td>
                <td colspan = 2>Actions</td>
            </tr>
        </thead>
        <tbody>
            @foreach($purchases as $key => $purchasing)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{date('d-m-Y', strtotime($purchasing->created_at))}}</td>
                <td>{{ $purchasing->nomor_masuk }}</td>
                <td>{{$purchasing->distributor_nama}}</td>
                <td>{{$purchasing->nomor_faktur}}</td>
                <td>{{date('d-m-Y', strtotime($purchasing->tanggal_faktur))}}</td>
                <td>{{ number_format($purchasing->aktif, 2, ',', '.') }}</td>
                <td>
                    <a href="{{ route('purchasing.edit',$purchasing->id)}}" class="btn btn-primary">Edit</a>
                </td>
                <td>
                    <form action="{{ route('purchasing.destroy', $purchasing->id)}}" method="post">
                      @csrf
                      @method('DELETE')
                      <button class="btn btn-danger" type="submit" onclick="if(confirm('Lanjutkan hapus data?')) {return true;} else {return false;}">Hapus</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
        </table>
    <div>
</div>
@endsection