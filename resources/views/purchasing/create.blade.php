@extends('layouts.app')

@section('content')
<div class="row">
 	<div class="col-sm-6 offset-sm-1">
    	<h1 class="display-3">Tambah pembelian</h1>
  		<div>
    		@if ($errors->any())
      			<div class="alert alert-danger">
        		<ul>
            		@foreach ($errors->all() as $error)
              		<li>{{ $error }}</li>
            		@endforeach
        		</ul>
      			</div><br />
    		@endif
    	</div>
    </div>
 	<div class="col-sm-10 offset-sm-1">
 		<div>
      		<form method="post" action="{{ route('distributors.store') }}">
          		@csrf
          		<div class="form-group">    
              		<label for="nomor_masuk" class="col-sm-4">Nomor Pembelian:</label>
              		<div class="col-sm-8">
	              		<input type="text" class="form-control" name="nomor_masuk"/>
              		</div>
	          	</div>
	          	<div class="form-group">    
              		<label for="distributor_id" class="col-sm-4">Distributor:</label>
              		<div class="col-sm-8">
	              		<select class="form-control" name="distributor_id">
	              			<option value="0">- Pilih Distributor -</option>
	              			@foreach($distributors as $distributor)
								<option value="{{$distributor->id}}">{{$distributor->nama}}</option>
	              			@endforeach
	              		</select>
	              	</div>
	          	</div>
	          	<div class="form-group">    
              		<label for="nomor_faktur" class="col-sm-4">Nomor Faktur:</label>
              		<div class="col-sm-8">
              			<input type="text" class="form-control" name="nomor_faktur"/>
              		</div>
	          	</div>
	          	<div class="form-group">    
              		<label for="tanggal_faktur" class="col-sm-4">Tgl Faktur:</label>
              		<div class="col-sm-8">
	              		<input type="text" class="form-control" name="tanggal_faktur"/>
	              	</div>
	          	</div>
          		<button type="button" id="btn_add_item" class="btn btn-primary">Tambah Item</button>
	          	<div style="max-width: 100%; overflow-x: scroll;">
	          		<table class="table table-striped" id="tbl-item">
			        <thead>
			            <tr>
			                <td>No</td>
			                <td>Item</td>
			                <td>Satuan</td>
			                <td>Harga Satuan</td>
			                <td>Jumlah</td>
			                <td>Sub-Total</td>
			                <td>Diskon (%)</td>
			                <td>Diskon (Rp.)</td>
							<td>Pajak (%)</td>
							<td>Pajak (Rp.)</td>
			                <td>Harga Netto</td>
							<td>Hapus</td>
			            </tr>
			        </thead>
			        <tbody>
			        </tbody>
			    	</table>
				</div>
				<div></div>
				<div class="form-group">
	          		<button type="submit" class="btn btn-primary">Simpan Pembelian</button>&emsp;
	          		<a href="{{route('purchasing.index')}}" class="btn btn-danger">Batal</a>
	          	</div>
	     	</form>
  		</div>
	</div>
</div>
@endsection

@section('footer_scripts')
<script type="text/javascript">
	$(document).ready(function(){ 
		$('#btn_add_item').on('click', function(){
			var jmlRow = $('#tbl-item tbody tr').length + 1;
			$('#tbl-item tbody').append('<tr><td class="urut">'+jmlRow+'</td>'+
				'<td><select class="input-sm item_id" name="detail[][\'item_id\']" id="item_id_'+jmlRow+'" onchange="set_satuan($(this));">'+
	              			'<option value="0">- Pilih Item -</option>'+
	              			@foreach($items as $item)
								'<option value="{{$item->id}}||{{$item->kemasan_besar}}">{{$item->nama}} - {{$item->merk}} ({{$item->model}})</option>'+
	              			@endforeach
	              		'</select></td>'+
				'<td><input type="text" name="detail[][\'satuan\']" id="satuan_'+jmlRow+'" class="input-sm satuan" readonly="true" tabIndex="-1" /></td>'+
				'<td><input type="number" name="detail[][\'harga_satuan\']" id="harga_satuan_'+jmlRow+'" class="input-sm harga_satuan text-right" value="0" onkeyup="hitung_total($(this));" /></td>'+
				'<td><input type="number" name="detail[][\'jml_masuk\']" id="jml_masuk_'+jmlRow+'" class="input-sm jml_masuk text-right" value="0" onkeyup="hitung_total($(this));" /></td>'+
				'<td><input type="number" name="detail[][\'sub_total\']" id="sub_total_'+jmlRow+'" class="input-sm sub_total text-right" value="0" readonly="true" tabIndex="-1" /></td>'+
				'<td><input type="number" name="detail[][\'diskon_persen\']" id="diskon_persen_'+jmlRow+'" class="input-sm diskon_persen text-right" value="0" onkeyup="hitung_total($(this));" /></td>'+
				'<td><input type="number" name="detail[][\'diskon_nominal\']" id="diskon_nominal_'+jmlRow+'" class="input-sm diskon_nominal text-right" value="0" readonly="true" tabIndex="-1" /></td>'+
				'<td><input type="number" name="detail[][\'pajak\']" id="pajak_'+jmlRow+'" class="input-sm pajak text-right" value="0" onkeyup="hitung_total($(this));" /></td>'+
				'<td><input type="number" name="detail[][\'pajak_nominal\']" id="pajak_nominal_'+jmlRow+'" class="input-sm pajak_nominal text-right" value="0" readonly="true" tabIndex="-1" /></td>'+
				'<td><input type="number" name="detail[][\'harga_netto\']" id="harga_netto_'+jmlRow+'" class="input-sm harga_netto text-right" value="0" /></td>'+
				'<td><button type="button" class="btn btn-danger btn-sm" onclick="hapus($(this))">Hapus</button></td></tr>');
			$('#tbl-item tbody tr:last-child').find('.item_id').focus();
		});

	});

	function hapus(el) {
		el.closest('tr').remove();
		urut_ulang();
	}

	function urut_ulang() {
		$.each($('.urut'), function(index,value) {
			$(this).html(index+1);
		})
	}

	function set_satuan(el) {
		var value = el.val().split('||');
		el.closest('tr').find('.satuan').val(value[1]);
		el.closest('tr').find('.harga_satuan').focus();
	}

	function hitung_total(el) {
		var hargaSatuan = parseFloat(el.closest('tr').find('.harga_satuan').val());
		var jumlah 		= parseFloat(el.closest('tr').find('.jml_masuk').val());
		var diskonPersen= parseFloat(el.closest('tr').find('.diskon_persen').val());
		var pajak		= parseFloat(el.closest('tr').find('.pajak').val());
		
		var subTotal 	= hargaSatuan * jumlah;
		var	diskonNum = (subTotal * diskonPersen / 100);
		var hargaTemp 	= subTotal - diskonNum;
		var pajakNum 	= (hargaTemp * pajak / 100);
		var hargaNetto 	= hargaTemp + pajakNum;

		el.closest('tr').find('.sub_total').val(subTotal);
		el.closest('tr').find('.diskon_nominal').val(diskonNum);
		el.closest('tr').find('.pajak_nominal').val(pajakNum);
		el.closest('tr').find('.harga_netto').val(hargaNetto);

		console.log('hargaSatuan: '+hargaSatuan);
		console.log('jumlah: '+jumlah);
		console.log('diskonPersen: '+diskonPersen);
		console.log('diskonNum: '+diskonNum);
		console.log('pajak: '+pajak);
		console.log('subTotal: '+subTotal);
		console.log('hargaTemp: '+hargaTemp);
		console.log('pajakNum: '+pajakNum);
		console.log('hargaNetto: '+hargaNetto);
	}
</script>
@stop