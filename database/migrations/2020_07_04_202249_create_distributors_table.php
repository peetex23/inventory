<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistributorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distributors', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('kode', 25);
            $table->string('nama', 100);
            $table->text('alamat')->nullable;
            $table->string('telp_kantor',13)->nullable;
            $table->string('narahubung', 50)->nullable;
            $table->string('telp_narahubung', 13)->nullable;
            $table->boolean('aktif');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distributors');
    }
}
