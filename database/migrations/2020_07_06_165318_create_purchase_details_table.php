<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_details', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('purchase_id');
            $table->foreignId('factory_id');
            $table->foreignId('item_id');
            $table->integer('jml_masuk');
            $table->float('harga_satuan',8,2);
            $table->float('diskon_persen',8,2);
            $table->float('diskon_nominal',8,2);
            $table->float('harga_netto',8,2);
            $table->float('pajak',8,2);
            $table->foreign('purchase_id')->references('id')->on('purchases');
            $table->foreign('factory_id')->references('id')->on('factories');
            $table->foreign('item_id')->references('id')->on('items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_details');
    }
}
