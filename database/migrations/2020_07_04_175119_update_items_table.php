<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('items', function (Blueprint $table) {
            $table->string('merk',25)->nullable()->change();
            $table->string('model',50)->nullable()->change();
            $table->text('spesifikasi')->nullable()->change();
            $table->string('satuan_kecil',15)->nullable()->change();
            $table->string('kemasan_besar',15)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
