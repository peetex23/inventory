<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('distributor_id');
            $table->string('nomor_masuk',10);
            $table->string('nomor_faktur',25);
            $table->date('tanggal_faktur');
            $table->boolean('status');
            $table->float('total_faktur',8,2);
            $table->float('total_pajak',8,2);
            $table->float('total_diskon',8,2);
            $table->foreign('distributor_id')->references('id')->on('distributors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
