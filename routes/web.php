<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('/items', 'ItemController')->middleware('auth');
Route::resource('/distributors', 'DistributorController')->middleware('auth');
Route::resource('/factories', 'FactoryController')->middleware('auth');
Route::resource('/purchasing', 'PurchasingController')->middleware('auth');